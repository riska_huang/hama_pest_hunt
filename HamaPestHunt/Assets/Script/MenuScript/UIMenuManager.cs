﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenuManager : MonoBehaviour {

    public GameObject menuPanel;
    public GameObject aboutPanel;
    public GameObject howToPanel;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Define the button onClick action
    /// Load the game scene when button clicked
    /// </summary>
    /// <param name="sceneIndex"></param>
    public void StartGame(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
        Time.timeScale = 1;
        Debug.Log("Start Game");
    }

    /// <summary>
    /// Define the button onClick action
    /// Set the UI for About Game to be Active
    /// </summary>
    public void AboutGame()
    {
        menuPanel.SetActive(false);
        aboutPanel.SetActive(true);
        Debug.Log("About Game");
    }

    /// <summary>
    /// Define the button onClick action
    /// Set the UI for How to Play to be Active
    /// </summary>
    public void HowToPlay()
    {
        menuPanel.SetActive(false);
        howToPanel.SetActive(true);
        Debug.Log("How to Play");
    }

    /// <summary>
    /// Define the button onClick action
    /// Quit the Application when the button is clicked
    /// </summary>
    public void ExitGame()
    {
        Debug.Log("Exit Game");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif 
    }

    /// <summary>
    /// Define the button onClick action
    /// Go back to the main menu when the button is clicked
    /// </summary>
    public void BackMenu()
    {
        aboutPanel.SetActive(false);
        howToPanel.SetActive(false);
        menuPanel.SetActive(true);
        Debug.Log("Back to Menu");
    }
}
