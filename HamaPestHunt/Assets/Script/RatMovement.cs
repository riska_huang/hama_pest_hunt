﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatMovement : MonoBehaviour {

    // Player Movement Variables/....
    public int movespeed = 2;
    public int maxDist = 10;
    public int tilt = 30;
    public GameObject frozenUI;

    private Rigidbody ratRb;
    private GameObject[] cheeseTarget;

    //Initialise variables
    private void Awake()
    {
        ratRb = this.GetComponent<Rigidbody>();
        cheeseTarget = GameObject.FindGameObjectsWithTag("Food");
    }

    // Use this for initialization
    void Start () {
        //GetComponent<Animator>().StartPlayback();
        //DontDestroyOnLoad(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        
        //rigidbody.MovePostion(rigidbody.position + transform.forward * speed * Time.deltaTime);
        //transform.Translate(enemyDirection * movespeed * Time.deltaTime);
        
    }

    private void FixedUpdate()
    {
        ratRb.MovePosition(this.GetComponent<Rigidbody>().position + transform.forward * movespeed * Time.deltaTime);
        MoveTowardCheese();
    }

    /// <summary>
    /// Define the collision between the rat and the obstacle and food (cheese)
    /// Redirect the rat movement when it collide with any obstacle
    /// Freeze the rat when it collide with the food (cheese)
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Obstacle")) 
        {
            if (ratRb.velocity.z > 0)
            {
                //ratRb.velocity = Vector3.zero;
                ratRb.MovePosition(ratRb.position - transform.forward);
            }
            else if (ratRb.velocity.z < 0)
            {
                //ratRb.velocity = Vector3.zero;
                ratRb.MovePosition(ratRb.position + transform.forward);
            }
            Quaternion rot = Quaternion.LookRotation(-this.transform.forward);
            //transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime);
            Quaternion ratRot = Quaternion.Slerp(transform.rotation, rot, tilt * Time.deltaTime);
            ratRb.MoveRotation(ratRot);
            //
            //
            collision.rigidbody.velocity = Vector3.zero;
            //collision.rigidbody.isKinematic = true;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;

        }

        else if(collision.gameObject.tag == "Food")
        {
            Destroy(collision.gameObject);
            Debug.Log("Collide with Cheese");
            this.GetComponent<AudioSource>().Play();
            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            this.GetComponent<Animator>().speed = 0;
            this.GetComponentInChildren<ParticleSystem>().Play();
            frozenUI.SetActive(true);
        }
    }

    /// <summary>
    /// Reset the rats velocity after colliding
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit(Collision collision)
    {
        //Quaternion rotExit = Quaternion.LookRotation()
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    /// <summary>
    /// Redirect the rats toward the cheese when there are cheese nearby
    /// </summary>
    public void MoveTowardCheese()
    {
        if(cheeseTarget == null)
        {
            Debug.Log("Empty Cheese Target");
            return;
        }

        foreach(GameObject cheese in cheeseTarget)
        {
            var distance = Vector3.Distance(transform.position, cheese.transform.position);
            if(distance <= maxDist)
            {
                Quaternion cheeseRot = Quaternion.LookRotation(cheese.transform.position - transform.position);
                Quaternion cheeseRotate = Quaternion.Slerp(transform.rotation, cheeseRot, tilt* Time.deltaTime);
                ratRb.MoveRotation(cheeseRotate);
            }
        }
    }
}
