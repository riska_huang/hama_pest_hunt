﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseCollision : MonoBehaviour {
    private Rigidbody rigidBody;
    //private RigidbodyConstraints freeze;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        //freeze = rigidBody.constraints;
    }

    // Use this for initialization
    void Start () {
        //DontDestroyOnLoad(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Define collision between the cheese and the floor.
    /// Stop all cheese movement when cheese collide with the floor
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            rigidBody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
