﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetRatCollision : MonoBehaviour {

    private GameObject gameManagerObj;
    private Rigidbody rigidBody;
    private EnemyManager enemyManager;

    //Initialise variables
    void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);
        gameManagerObj = GameObject.FindGameObjectWithTag("GameController");
        enemyManager = gameManagerObj.GetComponentInChildren<EnemyManager>();
        rigidBody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Define collision between the net with the enemies (rat) and the ground (floor)
    /// Stop all movement when the net collide with the ground
    /// Kill the Rat when the net collide with any rats in the scene
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        //check if the enemy hit the net
        if (collision.gameObject.tag == "Enemy")
        {
            enemyManager.KillRat(collision.gameObject);
            //Destroy(collision.gameObject);

            PlayParticle();
            PlayNetAudio();
        }

        if (collision.gameObject.tag == "Ground")
        {
            rigidBody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    /// <summary>
    /// Define the particle that will be played when the net collide with the rats
    /// </summary>
    public void PlayParticle()
    {
        ParticleSystem[] particle = GetComponentsInChildren<ParticleSystem>();
        foreach (var par in particle)
        {
            par.Play();
        }
    }

    /// <summary>
    /// Define the audio to be played when the net collide with the rats
    /// </summary>
    public void PlayNetAudio()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
}
