﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //public initialisation
    public GameObject netPrefab;
    public GameObject cheesePrefab;
    public GameObject emitNetPos;
    public GameObject emitCheesePos;
    public GameObject[] handCarry;

    public float forwardForce = 30;
    public float tilt = 10;

    //private initalisation
    private Rigidbody playerRb;
    private RigidbodyConstraints oriConstrains;
    private int curWeapon;

    private void Awake()
    {
        playerRb = GetComponent<Rigidbody>();
        oriConstrains = playerRb.constraints;
    }

    // Use this for initialization
    void Start () {
        //DontDestroyOnLoad(this.gameObject);
        curWeapon = 0;
        handCarry[0].SetActive(true);
        handCarry[1].SetActive(false);
	}
	
	// Update is called once per frame
    /// <summary>
    /// All keyboard and mouse controller are defined here
    /// </summary>
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(curWeapon == 0)
                NetBullet();
            else if(curWeapon == 1)
                CheeseBullet();
        }
            
        if (Input.GetButtonDown("Fire1"))
        {
            if (curWeapon == 0)
                NetBullet();
            else if (curWeapon == 1)
                CheeseBullet();
        }

        if (Input.GetMouseButtonDown(1)) //right click
        {
            if (curWeapon == 0)
                curWeapon = 1;
            else if (curWeapon == 1)
                curWeapon = 0;
            ChangeHandCarry(curWeapon);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (curWeapon == 0)
                curWeapon = 1;
            else if (curWeapon == 1)
                curWeapon = 0;
            ChangeHandCarry(curWeapon);
        }
    }


    private void FixedUpdate()
    {
        PlayerMovement();
    }

    /// <summary>
    /// Control the keyboard movement of the player
    /// </summary>
    private void PlayerMovement()
    {
        //transform.position = Camera.main.transform.position;
        //var x = Input.GetAxis("Horizontal") * Time.deltaTime * 300.0f;
        //var z = Input.GetAxis("Vertical") * Time.deltaTime * 5.0f;

        //Camera.main.transform.Rotate(0, x, 0);
        //Camera.main.transform.Translate(0, 0, z);

        var inputX = Input.GetAxis("Horizontal");
        var inputY = Input.GetAxis("Vertical");
        //Debug.Log(inputY);

        if (inputY == 0 && inputX == 0)
        {
            playerRb.constraints = RigidbodyConstraints.FreezeAll;
        }
        else
        {
            playerRb.constraints = oriConstrains;
            Vector3 movement = new Vector3(inputX, 0.0f, inputY);
            playerRb.velocity = movement * forwardForce;

            playerRb.position = new Vector3(playerRb.position.x, 0.0f, playerRb.position.z);
            playerRb.MovePosition(playerRb.position + transform.forward * inputY * forwardForce * Time.deltaTime);

            //if(inputY != 0) {
            //    playerRb.velocity = Vector3.zero;
            //    playerRb.MoveRotation(Quaternion.LookRotation(new Vector3(0.0f, playerRb.velocity.x * tilt, 0.0f)));
            //    //Camera.main.transform.Rotate(playerRb.rotation.x, playerRb.rotation.y, playerRb.rotation.z);
            //}
            
            Camera.main.transform.Rotate(0.0f, playerRb.velocity.x, 0.0f);
            
        }
    }

    /// <summary>
    /// Clone the net prefab
    /// Shoot the net with predetermined direction and speed
    /// </summary>
    void NetBullet()
    {
        PlayShootAudio();
        //clone the net
        GameObject tempNet;
        tempNet = Instantiate(netPrefab, emitNetPos.transform.position, emitNetPos.transform.rotation) as GameObject;

        //correct the net rotation
        //tempNet.transform.Rotate(Vector3.left * 90);

        Rigidbody tempRb;
        tempRb = tempNet.GetComponent<Rigidbody>();

        //tempRb.AddForce(emitPos.transform.forward * forwardForce);
        tempRb.velocity = emitNetPos.transform.forward * forwardForce;

        Destroy(tempNet, 3.0f);
    }

    /// <summary>
    /// The audio will play when the net is shot.
    /// Define to play audio here
    /// </summary>
    void PlayShootAudio()
    {
        AudioSource audio = GetComponentInChildren<AudioSource>();
        audio.Play();
    }

    /// <summary>
    /// Define the collision between the player and all obstacle here
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            if(playerRb.velocity.z > 0)
            {
                playerRb.velocity = Vector3.zero;
                playerRb.MovePosition(playerRb.position - transform.forward);
            }
            else if(playerRb.velocity.z < 0)
            {
                playerRb.velocity = Vector3.zero;
                playerRb.MovePosition(playerRb.position + transform.forward);
            }
            //playerRb.MovePosition(-playerRb.position);
        }
    }

    /// <summary>
    /// Define the action to switch handcarry between the net and cheese
    /// </summary>
    /// <param name="index"></param>
    void ChangeHandCarry(int index)
    {
        for (int i = 0; i < handCarry.Length; i++)
        {
            if (i == index)
            {
                handCarry[i].SetActive(true);
            }
            else
            {
                handCarry[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// Clone the cheese prefab
    /// Shoot the cheese with predetermined direction and speed
    /// </summary>
    void CheeseBullet()
    {
        //handCarry[1].SetActive(false);
        //PlayShootAudio();
        //clone the net
        GameObject tempCheese;
        tempCheese = Instantiate(cheesePrefab, emitCheesePos.transform.position, emitCheesePos.transform.rotation) as GameObject;

        tempCheese.transform.Rotate(Vector3.up * 180);

        Rigidbody tempRb;
        tempRb = tempCheese.GetComponent<Rigidbody>();

        //tempRb.AddForce(-emitCheesePos.transform.forward * 1);
        tempRb.velocity = emitCheesePos.transform.forward * 1;
    }
}
