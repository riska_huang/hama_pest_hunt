﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //public variables
    public float minX = -60.0f;
    public float maxX = 60.0f;
    public float minY = -360.0f;
    public float maxY = 360.0f;

    public float senX = 15f;
    public float senY = 15f;

    public Camera camera;

    //private variables
    private float rotX = 0f;
    private float rotY = 0f;

    // Use this for initialization
    void Start () {
        //Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {

        rotY += Input.GetAxis("Mouse X") * senY;
        //rotX += Input.GetAxis("Mouse X") * senX;

        rotX = Mathf.Clamp(rotX, minX, maxX);

        camera.transform.localEulerAngles = new Vector3(0, rotY, 0);

        //if (Input.GetKey(KeyCode.Escape))
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //    Cursor.visible = true;
        //}
	}
}
