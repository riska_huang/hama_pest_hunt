﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //state
    enum State { NotStarted, Playing, Pause, GameOver, WonGame }

    //current state
    State currState;

    //Enemy Manager
    EnemyManager enemyManager;
    UIManager ui;

    void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);
        enemyManager = GetComponentInChildren<EnemyManager>();
        ui = GetComponentInChildren<UIManager>();
    }

    // Use this for initialization
    void Start () {
        InitGame();
        ContinueGame();
	}

    /// <summary>
    ///action when the game not yet started
    /// </summary>
    public void NotStarted()
    {
        if (currState == State.NotStarted) return;
        currState = State.NotStarted;
    }

    /// <summary>
    /// Initialise the game.
    /// Set the state.
    /// Summon Enemy
    /// </summary>
    public void InitGame()
    {
        //check the current state
        if (currState == State.Playing) return;

        // set the state
        currState = State.Playing;

        // initiate enemy
        enemyManager.SummonRat();
    }

    /// <summary>
    /// Pause the game when pop up windows appear
    /// </summary>
    public void PauseGame()
    {
        if (currState == State.Pause) return;

        currState = State.Pause;

        ui.popUpCanvas.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    /// <summary>
    /// Continue the game when pop up windows dissapear
    /// </summary>
    public void ContinueGame()
    {
        if (currState == State.Playing) return;
        currState = State.Playing;

        ui.popUpCanvas.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    /// <summary>
    /// Set the state to Game Over
    /// Summon score
    /// </summary>
    public void GameOver()
    {
        if (currState == State.GameOver) return;

        currState = State.GameOver;
        ui.gameOverPanel.SetActive(true);
        Debug.Log("Game Over");
    }

    /// <summary>
    /// Set the state to WonGame
    /// Summon Score
    /// </summary>
    public void WonGame()
    {
        if (currState == State.WonGame) return;
        currState = State.WonGame;
        ui.winPanel.SetActive(true);
        Debug.Log("You Win!");
    }
}