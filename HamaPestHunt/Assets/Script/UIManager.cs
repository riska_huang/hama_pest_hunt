﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public Text scoreText;
    public Text gameOverScoreText;
    public Text winScoreText;
    public Canvas popUpCanvas;
    public GameObject gameOverPanel;
    public GameObject winPanel;
    private GameManager gameManager;

    //initialise variable
    private void Awake()
    {
        gameManager = GetComponentInParent<GameManager>();
    }

    // Use this for initialization
    void Start () {
        scoreText.text = "0000";
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    /// <summary>
    /// Update the UI score text based on given value
    /// </summary>
    /// <param name="score"></param>
    public void UpdateScore(int score)
    {
        //+20 per rat
        scoreText.text = score.ToString();
        gameOverScoreText.text = "Final Score: " + score.ToString(); 
        winScoreText.text = "Final Score: " + score.ToString();
    }

    /// <summary>
    /// Define the button onClick action
    /// Pause the game and call give up popUp panel when the button is clicked
    /// </summary>
    public void GiveUpGame()
    {
        Debug.Log("Give Up");
        gameManager.PauseGame();
    }

    /// <summary>
    /// Define the button onClick action
    /// Load the Menu Scene when the button is clicked
    /// </summary>
    /// <param name="sceneIndex"></param>
    public void CallMenu(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
        Debug.Log("Go to Menu");
    }
}
