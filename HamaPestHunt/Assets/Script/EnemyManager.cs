﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public GameObject rat;

    public int numRat;

    private int numScore;

    public UIManager uiManager;
    public GameManager gameManager;

    public void Start()
    {
        //DontDestroyOnLoad(this.gameObject);
        numScore = 0;
        gameManager = GetComponentInParent<GameManager>();
    }

    /// <summary>
    /// Clone and instantiate rat to become enemy
    /// </summary>
    public void SummonRat()
    {
        for (int i = 0; i < numRat; i++)
        {
            Vector3 position = new Vector3(Random.Range(5, 79), 0.005f, Random.Range(5, 100));
            Quaternion rotation = new Quaternion(0, Random.Range(-170, 170),0,0);
            Instantiate(rat, position, rotation);
            //rat.transform.Rotate(Vector3.up, Random.Range(-170, 170));

        }
    }

    /// <summary>
    /// Add 20 for the score each time kill a rat
    /// Update the score UI
    /// </summary>
    public void CalculateScore()
    {
        numScore = numScore + 20;
        uiManager.UpdateScore(numScore);
    }

    /// <summary>
    /// Kill and Destroy rat that collide with the net
    /// Killing effect goes here
    /// </summary>
    /// <param name="gameObject"></param>
    public void KillRat(GameObject gameObject)
    {
        Destroy(gameObject);
        numRat = numRat - 1;
        CalculateScore();
        
        if(numRat == 0)
        {
            gameManager.WonGame();
        }
    }
}
